<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Ipads extends Model
{
    protected $fillable = ['name','price','describe'];
}
