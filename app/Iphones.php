<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iphones extends Model
{
    protected $fillable = ['name','price','describe'];
}
