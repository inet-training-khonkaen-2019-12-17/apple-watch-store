<?php

namespace App\Http\Controllers;

use App\Iphones;
use App\Onechat;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class IphoneController extends Controller
{
    public function showProduct(){
        $product = Iphones::all();
        return view('products.iphone',compact('product'));
    }
    public function addProductIphone(Request $request)
    {
        return view('products.create_iphone');
    }
    public function createProductIphone(Request $request)
    {
        $product = new Iphones;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->url_pic = $request->url_pic;
        $product->describe = $request->describe;

        if (!$product->save()) {
            return redirect()->back()->with(['status' => 'failed!']);
        }
        return redirect()->route('iphone.page');
    }
    public function editProductIphone($id)
    {
        $product = Iphones::find($id);
        // dd($product);
        return view('products.edit_iphone',compact('product'));
    }

 public function update(Request $request)
    {

    //    dd($id);
        $product = Iphones::find($request->id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->url_pic = $request->url_pic;
        $product->describe = $request->describe;
        if (!$product->save()) {
            return redirect()->back()->with(['status' => 'Failed!']);
        }
        return redirect()->route('iphone.page');

    }
    public function delete($id)
    {
        $product = Iphones::find($id);
        $product->delete();
        return redirect()->route('iphone.page');
    }
    public function describe($id)
    {
        $product = Iphones::find($id);
        $user = Auth::user()->onechat;
        return view('products.detail_iphone')->with(compact('product', 'user'));
    }
    public function storeOneChat(Request $request)
    {   
        $info = $this->checkOneChatUser($request->one_mail);
        if (!$info->status === 'fail') {
            return redirect()->back()->with(['status' => 'fail']);
        }
        $onechat = new Onechat();
        $onechat->one_mail = $request->one_mail;
        $onechat->onechat_id = $info->friend->user_id;
        $onechat->user_id = Auth::user()->id;

        if (!$onechat->save()) {
            return redirect()->back()->with(['status' => 'Save Fail']);
        }
        // dd($onechat);
        $this->sendMessage('สวัสดีแก้ไขโปรไฟล์เรียบร้อยแล้ว', $onechat->onechat_id);
        return redirect()->back();
    }


    private function checkOneChatUser($email)
    {
    try {
        // dd($client);
        $client = new Client();
        $res = $client->request('POST', "https://chat-manage.one.th:8997/api/v1/searchfriend", [
            "headers" => [
                'Authorization' => "Bearer A8ec5bad36ef85b898db931036941de6d99a2d1690a7847d197e1334f5ba2d172b124007e59164ef08c568cdeb5ef2eee",
                "Content-Type" => "application/json",
            ],
            'json' => [
                'bot_id' => "Bf0261d62c7ae5772b7701a12df09e066",
                "key_search" => $email
            ]
        ]);
        $resToJson = json_decode($res->getBody()->getContents());
        return $resToJson;
    } catch (GuzzleException $e) {
       return (object) ['status' => 'fail'];
    }
    }
    private function sendMessage($msg, $onechat_id)
    {
    try {
        $client = new Client();
        $res = $client->request('POST', "https://chat-public.one.th:8034/api/v1/push_message", [
            "headers" => [
                'Authorization' => "Bearer A8ec5bad36ef85b898db931036941de6d99a2d1690a7847d197e1334f5ba2d172b124007e59164ef08c568cdeb5ef2eee",
                "Content-Type" => "application/json",
            ],
            'json' => [
                "to" => $onechat_id,
                "bot_id" => "Bf0261d62c7ae5772b7701a12df09e066",
                "type" => "text",
                "message" => "สินค้าของคุณได้ยืนยันการสั่งซื้อแล้ว"
            ]
        ]);
        $resToJson = json_decode($res->getBody()->getContents());
        return $resToJson;
    } catch (GuzzleException $e) {
       return (object) ['status' => 'fail'];
    }
    }
}
