<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function loginPage()
    {
        return view('auth.login');
    }
    public function login(Request $request)
    {
        $user = User::where('email', $request->email)
            ->get()->first();
        
        // Check Error
        // if (Hash::check($request->password, $user->password)) {
        //     // แบบที่1.1
        //     // Auth::login($user);

        //     // แบบที่ 1.2 
        //     Auth::loginUsingId($user->id);
        //     return redirect('/');
        // }
        $isAuth = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ]);
        if (!$isAuth) {
            return redirect()->back()->with(['status' => 'Login failed!']);
        }
        return redirect('/applewatch');
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
    public function registerPage()
    {   
        return view('auth.register');
    }
    public function register(RegisterRequest $request)
    {
        $userExist = User::where('email', $request->email)->exists();
        
        if ($userExist) {
            return redirect()->back()->with(['status' => 'user is already existing!']);
        }

        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);

        if (!$user->save()) {
            return redirect()->back();
        }
        return redirect()->route('login');


        // $validateData = $request->validate([
        //     'name' => 'required|max:255',
        //     'email' => 'required|email|max:255',
        //     'password' => 'required|min:6|confirmed'
        // ]);
        // dd($validateData);
    }
    // public function Userss()
    // {
    //     $user = User::find($id)
    //     return view('products.detail_applewatch', compact('user'));
    // }
}
