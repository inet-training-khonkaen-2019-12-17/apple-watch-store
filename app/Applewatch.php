<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applewatch extends Model
{
    protected $fillable = ['name', 'price', 'describe'];
}
