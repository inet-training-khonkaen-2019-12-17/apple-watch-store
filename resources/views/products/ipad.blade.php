@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

                <div class=" text-center" style="font-size: 40px">iPad</div>
                <div align="right">
                <a href="{{ route('ipad.create.page') }}"><button type="button" class="btn btn-dark"><i class="fas fa-plus"> เพิ่มสินค้า</i></button></a>
                </div>
                <!-- <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Try in this section ...
                </div> -->
                <div class="container">
                <b style="font-size: 25px"></b>
                <br> <br> <br>
  <div class="row" >
    @if (count($product) == 0)
    <div class="col-sm-12 text-center">
    <div class="card">
        <div class="card-body">
          No Data.
        </div>
      </div>
    </div>
    @endif
    @foreach ($product as $product)

    <div class="col-sm-4" style="padding:10px;">
        <div align="right">
            <a href="{{ route('edit.ipad.page',$product->id) }}"><button type="button" class="btn btn-light"><i class="material-icons">mode_edit</i></button></a>
            <button type="button" class="btn btn-light" data-toggle="modal" data-target="#delete{{$product->id}}"><i class="material-icons">delete</i></button>

        </div>
        <div class="text-center">
        <img src="{{ $product->url_pic }}" class="rounded " style="height:190px">
        </div>
    <div class="text-center mt-5" style="font-size: 20px">{{ $product->name }}</div>
    <div class="text-center" style="font-size: 14px">ราคาเริ่มต้น {{ $product->price }} </div>
    <div class="text-center " style="font-size: 16px">
        <?php echo substr($product->describe,0,250)."..." ?>
    </div>
    <div class="text-center">
    <a href="{{ route('ipad.detail.page',$product->id) }}">
    <button class="btn btn-primary">ซื้อ</button>
    </a>
    </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="delete{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">ยืนยันการลบ</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            ต้องการลบสินค้า {{ $product->name }} หรือไม่
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
            <a href="{{ route('ipad.delete',$product->id) }}"><button type="button" class="btn btn-light"><i class="material-icons">delete</i></button>ตกลง</button></a>
          </div>
        </div>
      </div>
    </div>
    <!----end modal ---->
    @endforeach
    </div>
</div>
            </div>

    </div>
</div>
@endsection
