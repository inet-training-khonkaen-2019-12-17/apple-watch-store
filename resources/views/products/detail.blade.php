@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <br>
            <div class=" text-left" style="font-size: 40px">รายการสั่งซื้อ</div>
            <br>
            <div class="container">
                <br> <br>
                <div class="row">
                    <div class="col-sm-5 text-center">
                        <img src="{{$product->url_pic}}" class="rounded " style="height:250px">
                    </div>
                    <div class="col-sm-7">
                        <div class=" mt-5" style="font-size: 26px">รายละเอียดสินค้า</div>
                        <div  style="font-size: 20px">{{ $product->name }}</div>
                        <div  style="font-size: 16px"><h5>ราคาเริ่มต้น {{ $product->price }} บาท</h5></div>
                            <div  style="font-size: 14px">{{ $product->describe }}</div>
                        <br>
                        <form action="{{ route('onechat.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                                <label for="one-email">One Mail</label>
                                <input type="email" name="one_mail" class="form-control" id="one-email">
                        </div>
                        <br>
                        <div style="font-size: 14px">
                            <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">ยืนยันการสั่งซื้อ</button>
                        </div>
                        </form>
                        <div style="font-size: 14px">
                        <a href="{{ route('applewatch.page') }}"><button class="btn btn-danger">ยกเลิกการสั่งซื้อ</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">ยืนยันการสั่งซื้อ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                ต้องการสั่งซื้อสินค้า {{ $product->name }} ราคา {{ $product->price }} บาท หรือไม่
              </div>
              <div class="modal-footer">
                {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                <a href="{{ route('products.ipad.page') }}"><button type="button" class="btn btn-primary">ตกลง</button></a>
              </div>
            </div>
          </div>
        </div>
        <!----end modal ---->
    </div>
@endsection
