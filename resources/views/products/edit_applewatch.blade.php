@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Product</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ route('applewatch.edit') }}" method="POST">
                        @csrf
                        <input type="hidden" value="{{ $apple->id }}" name="id">
                        <div class="form-group">
                            <label for="name">Name :</label>
                            <input type="text" name="name" class="form-control" id="name" value="{{ $apple->name }}">
                        </div>

                        <div class="form-group">
                            <label for="describe">Describe:</label>
                            <textarea class="form-control" name="describe" id="describe" rows="3" value="{{ $apple->describe }}">{{ $apple->describe }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="price">Price :</label>
                            <input type="number" name="price" class="form-control" id="price" value="{{ $apple->price}}">
                        </div>
                        <div class="form-group">
                            <label for="url_pic">URL Picture :</label>
                            <input type="text" name="url_pic" class="form-control" id="url_pic" value="{{ $apple->url_pic}}">
                        </div>

                        <div align="right">
                            <a href="{{ route('applewatch.page') }}" class="btn btn-danger">Back</a>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                    {{--  <div class="form-group">
                        <button type="submit" class="btn btn-success">
                            Submit
                        </button>
                            <a href="{{ route('book.page') }}">
                                <button class="btn btn-secondary">
                                    Cancel
                                </button>
                                </a>
                        </div>  --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
