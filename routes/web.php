<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::group(['middleware' => ['user']], function (){ 
    Route::get('/products/create', 'AppleWatchController@createPage')->name('products.create.page');
    Route::post('/products/create', 'AppleWatchController@create')->name('products.create');
    Route::get('/edit_applewatch/{id}', 'AppleWatchController@editAppleWatchPage')->name('applewatch.edit.page');
    Route::post('/edit_applewatch', 'AppleWatchController@update')->name('applewatch.edit');
    Route::get('/applewatch/detail/{id}', 'AppleWatchController@detail')->name('applewatch.detail.page');
    Route::get('/apple/delete/{id}','AppleWatchController@delete')->name('applewatch.delete');
    Route::post('/onechat/store', 'AppleWatchController@storeOneChat')->name('onechat.store');

    Route::get('/create_mac', 'MacController@addProductMac')->name('mac.create.page');
    Route::post('/create_mac', 'MacController@createProductMac')->name('mac.create');
    Route::get('/edit_mac/{id}', 'MacController@editProductMac')->name('mac.edit.page');
    Route::post('/edit_mac', 'MacController@update')->name('mac.edit');
    Route::get('/product_mac/delete/{id}','MacController@delete')->name('mac.delete'); // name คือชื่อของ route
    Route::get('/product_mac/detail/{id}', 'MacController@detail')->name('mac.detail.page');
    Route::post('/onechat/store', 'MacController@storeOneChat')->name('onechat.store');

    Route::get('/AddProduct','ProdductIpadController@addProductIpad')->name('ipad.create.page');
    Route::post('/AddProduct','ProdductIpadController@createProductIpad')->name('ipad.create');
    Route::get('/EditIpad/{id}', 'ProdductIpadController@editProductIpad')->name('edit.ipad.page');
    Route::post('/EditProduct', 'ProdductIpadController@update')->name('ipad.edit');
    Route::get('/ShowProduct/delete/{id}','ProdductIpadController@delete')->name('ipad.delete');
    Route::get('/ShowProduct/detail/{id}', 'ProdductIpadController@describe')->name('ipad.detail.page');
    Route::post('/onechat/store', 'ProdductIpadController@storeOneChat')->name('onechat.store');

    Route::get('/iphonedetail','IphoneController@iphonedetail')->name('detail.iphone.page');
    Route::get('/AddProduct','IphoneController@addProductIphone')->name('iphone.create.page');
    Route::post('/AddProduct','IphoneController@createProductIphone')->name('iphone.create');
    Route::get('/EditIphone/{id}', 'IphoneController@editProductIphone')->name('edit.iphone.page');
    Route::post('/EditProduct', 'IphoneController@update')->name('iphone.edit');
    Route::get('/ShowProduct/delete/{id}','IphoneController@delete')->name('iphone.delete');
    Route::get('/ShowProduct/detail/{id}', 'IphoneController@describe')->name('iphone.detail.page');
    Route::post('/onechat/store', 'IphoneController@storeOneChat')->name('onechat.store');

});
Route::get('/applewatch', 'AppleWatchController@AppleWatchPage')->name('applewatch.page');
Route::get('/mac', 'MacController@show')->name('mac.page');
Route::get('/ipad','ProdductIpadController@showProduct')->name('products.ipad.page');
Route::get('/iphone','IphoneController@showProduct')->name('iphone.page');
Route::get('/login', 'UserController@loginPage')->name('login.page');
Route::post('/login', 'UserController@login')->name('login');
Route::post('/logout', 'UserController@logout')->name('logout');
Route::get('/register', 'UserController@registerPage')->name('register.page');
Route::post('/register', 'UserController@register')->name('register');

// Route::get('/Detail','ProdductIpadController@detailProduct')->name('detail.ipad.page');


